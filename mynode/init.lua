minetest.register_node("mynode:connor",{
  description = "connor block",
  tiles = {"connor.png"},
  paramtype2 = 'facedir',
  groups = {oddly_breakable_by_hand = 2},
})

minetest.register_node("mynode:ember",{
  description = "ember block",
  tiles = {"ember.png"},
  paramtype2 = 'facedir',
  groups = {oddly_breakable_by_hand = 2},
})
