
# minetest-mod-examples

Copyright Kris Occhipinti 2024-02-04

(https://filmsbykris.com)

License GPLv3

```bash
mkdir -p ~/.minetest/mods
cp -vr mynode ~/.minetest/mods
```
